# Usage

```
git clone git@gitlab.com:StuHalliburton/vim.git ~/.vim
ln -s ~/.vim/.vimrc ~/.vimrc
```

Download Vundle to install Plugins

```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

Open `vim` and install the plugins

```
:PluginInstall
```
